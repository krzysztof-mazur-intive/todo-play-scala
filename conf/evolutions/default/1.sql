# --- !Ups

CREATE TABLE categories (
  id   BIGSERIAL,
  name CHARACTER VARYING(255) NOT NULL,
  PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE categories;
