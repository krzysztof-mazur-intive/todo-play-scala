# --- !Ups

CREATE TABLE todos (
  id          BIGSERIAL,
  name        CHARACTER VARYING(255) NOT NULL,
  description TEXT                   NOT NULL,
  category_id BIGINT                 NOT NULL REFERENCES categories (id),
  due_date    TIMESTAMP(0)           NOT NULL,
  created     TIMESTAMP(0)           NOT NULL,
  PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE todos;
