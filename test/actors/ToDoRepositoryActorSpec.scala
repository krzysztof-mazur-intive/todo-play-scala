package actors

import java.time.LocalDateTime

import actors.ToDoRepositoryActor.{Create, Delete, GetAll, GetOneById}
import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import model.{ToDo, ToDoRepository}
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Matchers._
import org.scalatest.junit.JUnitRunner

import scala.concurrent.Future

@RunWith(classOf[JUnitRunner])
class ToDoRepositoryActorSpec extends TestKit(ActorSystem("ToDoRepositoryActorSpec")) with WordSpecLike
  with BeforeAndAfterAll with MockitoSugar {

  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  "ToDoRepositoryActorSpec" should {

    "return seq of tasks" in {
      val dto = Seq(ToDo(id = Some(1), name = "name", description = "desc", categoryId = 1, dueDate = LocalDateTime.now()))
      val repository = mock[ToDoRepository]
      when(repository.findAll(None)).thenReturn(Future.successful(dto))

      val probe = TestProbe()
      val actor = system.actorOf(ToDoRepositoryActor.props(repository))

      actor.tell(GetAll(categoryId = None), probe.ref)

      val response = probe.expectMsgType[Seq[ToDo]]

      assert(response.lengthCompare(1) === 0)
      assert(response.head == dto.head)
      verify(repository, times(1)).findAll(None)
    }

    "return seq of tasks in category" in {
      val dto = Seq(ToDo(id = Some(1), name = "name", description = "desc", categoryId = 1, dueDate = LocalDateTime.now()))
      val repository = mock[ToDoRepository]
      when(repository.findAll(Some(1))).thenReturn(Future.successful(dto))

      val probe = TestProbe()
      val actor = system.actorOf(ToDoRepositoryActor.props(repository))

      actor.tell(GetAll(categoryId = Some(1)), probe.ref)

      val response = probe.expectMsgType[Seq[ToDo]]

      assert(response.lengthCompare(1) === 0)
      assert(response.head == dto.head)
      verify(repository, times(1)).findAll(Some(1))
    }

    "return tasks by id" in {
      val dto = Some(ToDo(id = Some(1), name = "name", description = "desc", categoryId = 1, dueDate = LocalDateTime.now()))
      val repository = mock[ToDoRepository]
      when(repository.findOneById(1)).thenReturn(Future.successful(dto))

      val probe = TestProbe()
      val actor = system.actorOf(ToDoRepositoryActor.props(repository))

      actor.tell(GetOneById(id = 1), probe.ref)

      val response = probe.expectMsgType[Option[ToDo]]

      assert(response.isDefined)
      assert(response.get == dto.get)
      verify(repository, times(1)).findOneById(1)
    }

    "return none when task with id not exists" in {
      val dto = None
      val repository = mock[ToDoRepository]
      when(repository.findOneById(5)).thenReturn(Future.successful(dto))

      val probe = TestProbe()
      val actor = system.actorOf(ToDoRepositoryActor.props(repository))

      actor.tell(GetOneById(id = 5), probe.ref)

      val response = probe.expectMsgType[Option[ToDo]]

      assert(response.isEmpty)
      verify(repository, times(1)).findOneById(5)
    }

    "insert new task" in {
      val repository = mock[ToDoRepository]
      when(repository.create(anyObject())).thenReturn(Future.successful(Some(1L)))

      val probe = TestProbe()
      val actor = system.actorOf(ToDoRepositoryActor.props(repository))

      actor.tell(Create(name = "n", description = "des", categoryId = 5L, dueDate = LocalDateTime.now()), probe.ref)

      val response = probe.expectMsgType[Option[Long]]

      assert(response.isDefined)
      assert(response.get === 1)
    }

    "delete task" in {
      val repository = mock[ToDoRepository]
      when(repository.delete(1)).thenReturn(Future.successful(1))

      val probe = TestProbe()
      val actor = system.actorOf(ToDoRepositoryActor.props(repository))

      actor.tell(Delete(id = 1), probe.ref)

      val response = probe.expectMsgType[Int]

      assert(response == 1)
    }

    "delete task which not exists" in {
      val repository = mock[ToDoRepository]
      when(repository.delete(1)).thenReturn(Future.successful(0))

      val probe = TestProbe()
      val actor = system.actorOf(ToDoRepositoryActor.props(repository))

      actor.tell(Delete(id = 1), probe.ref)

      val response = probe.expectMsgType[Int]

      assert(response == 0)
    }

  }
}
