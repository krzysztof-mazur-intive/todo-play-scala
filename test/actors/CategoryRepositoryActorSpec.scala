package actors

import actors.CategoryRepositoryActor.{Create, GetAll}
import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import model.{Category, CategoryRepository}
import org.junit.runner.RunWith
import org.mockito.Matchers.any
import org.scalatest.junit.JUnitRunner
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import play.api.cache.CacheApi
import org.mockito.Mockito._
import org.mockito.Matchers._

import scala.concurrent.Future

@RunWith(classOf[JUnitRunner])
class CategoryRepositoryActorSpec extends TestKit(ActorSystem("CategoryRepositoryActorSpec")) with WordSpecLike
  with BeforeAndAfterAll with MockitoSugar {

  override def afterAll: Unit = TestKit.shutdownActorSystem(system)

  "CategoryRepositoryActorSpec" should {

    "return seq of categories from cache" in {
      val dto = Category(id = Some(1), name = "category")
      val cache = mock[CacheApi]
      val repository = mock[CategoryRepository]
      when(cache.get[Future[Seq[Category]]](anyString())(any())).thenReturn(Some(Future.successful(Seq(dto))))

      val probe = TestProbe()
      val actor = system.actorOf(CategoryRepositoryActor.props(repository, cache))

      actor.tell(GetAll, probe.ref)

      val response = probe.expectMsgType[Seq[Category]]

      assert(response.lengthCompare(1) == 0)
      assert(response.contains(dto))
      verify(repository, times(0)).all()
    }

    "return seq of categories from repository" in {
      val dto = Category(id = Some(1), name = "category")
      val cache = mock[CacheApi]
      val repository = mock[CategoryRepository]
      when(cache.get[Future[Seq[Category]]](anyString())(any())).thenReturn(None)
      when(repository.all()).thenReturn(Future.successful(Seq(dto)))

      val probe = TestProbe()
      val actor = system.actorOf(CategoryRepositoryActor.props(repository, cache))

      actor.tell(GetAll, probe.ref)

      val response = probe.expectMsgType[Seq[Category]]

      assert(response.lengthCompare(1) == 0)
      assert(response.contains(dto))
      verify(repository, times(1)).all()
    }

    "insert new category and invalidate cache" in {
      val cache = mock[CacheApi]
      val repository = mock[CategoryRepository]
      when(repository.create(anyObject())).thenReturn(Future.successful(Some(1L)))

      val probe = TestProbe()
      val actor = system.actorOf(CategoryRepositoryActor.props(repository, cache))

      actor.tell(Create(name = "category"), probe.ref)

      val response = probe.expectMsgType[Option[Long]]

      assert(response.isDefined)
      assert(response.get == 1)
      verify(cache, times(1)).remove(anyString())
      verify(repository, times(1)).create(Category(name = "category"))
    }

  }

}
