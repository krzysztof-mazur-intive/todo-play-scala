package actors

import actors.JSONPlaceholderActor.GetPhotos
import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import dto.PhotoDto
import mockws.MockWS
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import play.api.libs.json.{Json, Writes}
import play.api.mvc.Action
import play.api.mvc.Results.Ok

@RunWith(classOf[JUnitRunner])
class JSONPlaceholderActorSpec extends TestKit(ActorSystem("JSONPlaceholderActorSpec")) with WordSpecLike
  with BeforeAndAfterAll {

  override def afterAll: Unit = TestKit.shutdownActorSystem(system)

  "JSONPlaceholderActor" should {

    "return seq of photos if was fetched" in {
      val dto = PhotoDto(id = 1, albumId = 1, title = "title", url = "url", thumbnailUrl = "thumb")
      val ws = MockWS {
        case ("GET", "https://jsonplaceholder.typicode.com/photos") => Action {
          implicit val photoWrites: Writes[PhotoDto] = Json.writes[PhotoDto]
          Ok(Json.toJson(Seq(dto)))
        }
      }

      val probe = TestProbe()
      val actor = system.actorOf(JSONPlaceholderActor.props(ws))

      actor.tell(GetPhotos, probe.ref)

      val response = probe.expectMsgType[Seq[PhotoDto]]

      assert(response.lengthCompare(1) == 0)
      assert(response.contains(dto))
    }

  }
}
