package utils

import java.sql.DriverManager

import com.spotify.docker.client.{DefaultDockerClient, DockerClient}
import com.whisk.docker.impl.spotify.SpotifyDockerFactory
import com.whisk.docker._

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.Try

trait DockerPostgresService extends DockerKit {

  private val client: DockerClient = DefaultDockerClient.builder().uri("unix:///var/run/docker.sock").build()

  override implicit val dockerFactory: DockerFactory = new SpotifyDockerFactory(client)

  val postgresAdvertisedPort = 5432
  val postgresExposedPort = 44444
  val postgresUser = "todo"
  val postgresPassword = "todo"
  val postgresDb = "todo"
  val postgresVersion = "9.6"
  val postgresImage = s"postgres:$postgresVersion"
  val postgresUrl = s"jdbc:postgresql://localhost:$postgresExposedPort/$postgresDb"
  val postgresDriverClass = "org.postgresql.Driver"

  val postgresContainer: DockerContainer = DockerContainer(postgresImage)
    .withPorts((postgresAdvertisedPort, Some(postgresExposedPort)))
    .withEnv(
      s"POSTGRES_USER=$postgresUser",
      s"POSTGRES_PASSWORD=$postgresPassword",
      s"POSTGRES_DB=$postgresDb"
    )
    .withReadyChecker(
      new PostgresReadyChecker(postgresUrl, postgresDriverClass, postgresUser, postgresPassword)
        .looped(30, 1.second)
    )

  override def dockerContainers: List[DockerContainer] = postgresContainer :: super.dockerContainers
}

class PostgresReadyChecker(url: String, driverClass: String, user: String, password: String)
  extends DockerReadyChecker {

  override def apply(container: DockerContainerState)(implicit docker: DockerCommandExecutor, ec: ExecutionContext): Future[Boolean] =
    Future.successful(Try {
      Class.forName(driverClass)
      DriverManager.getConnection(url, user, password).close()
      true
    }.getOrElse(false))
}