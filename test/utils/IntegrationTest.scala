package utils

import org.specs2.specification.BeforeAfterAll
import play.api.Application
import play.api.test.FakeApplication

trait IntegrationTest extends BeforeAfterAll with DockerPostgresService {

  private def inMemoryPGDatabase(): Map[String, String] = {
    Map(
      "slick.dbs.default.driver" -> "slick.driver.PostgresDriver$",
      "slick.dbs.default.db.profile" -> postgresDriverClass,
      "slick.dbs.default.db.url" -> postgresUrl,
      "slick.dbs.default.db.user" -> postgresUser,
      "slick.dbs.default.db.password" -> postgresPassword,
      "play.evolutions.db.default.autoApply" -> "true"
    )
  }

  def application: Application = {
    FakeApplication(additionalConfiguration = inMemoryPGDatabase())
  }

  override def beforeAll(): Unit = startAllOrFail()

  override def afterAll(): Unit = stopAllQuietly()
}
