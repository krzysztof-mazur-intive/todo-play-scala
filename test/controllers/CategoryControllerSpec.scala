package controllers

import controllers.json.CategoryJson
import dto.{CategoryDto, CreateCategoryDto}
import org.junit.runner.RunWith
import org.scalatest.Inside
import org.scalatest.junit.JUnitRunner
import org.scalatest.Matchers._
import play.api.libs.json.Json
import play.api.libs.ws._
import play.api.test._
import utils.IntegrationTest

@RunWith(classOf[JUnitRunner])
class CategoryControllerSpec extends PlaySpecification with IntegrationTest with CategoryJson with Inside {

  "CategoryController" should {

    "create and return list" in new WithServer(app = application) {

      private val createResponse = await(WS.url(s"http://localhost:$testServerPort/categories")
        .post(Json.toJson(CreateCategoryDto(name = "xyz"))))
      createResponse.status must equalTo(CREATED)

      private val response = await(WS.url(s"http://localhost:$testServerPort/categories").get())
      response.status must equalTo(OK)
      private val categories = Json.parse(response.body).as[Seq[CategoryDto]]

      assert(categories.lengthCompare(1) == 0)
      assert(categories.head.name.equals("xyz"))
    }

  }
}
