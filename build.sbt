name := "todo-play-scala"

version := "0.1"

scalaVersion := "2.11.12"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

routesGenerator := InjectedRoutesGenerator

val playVersion = "2.4.6"
val playSlickVersion = "1.1.1"
val slickVersion = "3.1.0"
val slickJodaMapperVersion = "2.1.0"
val pgDriverVersion = "9.4.1212"
val akkaVersion = "2.4.20"
val scalaOauth2Version = "0.16.1"
val scalatestVersion = "3.0.5"
val mockitoVersion = "1.10.19"
val h2Version = "1.4.196"
val mockWsVersion = "2.4.2"
val dockerTestkitVersion = "0.9.6"

libraryDependencies ++= Seq(
  cache,
  ws,
  "com.nulab-inc" %% "scala-oauth2-core" % scalaOauth2Version,
  "com.nulab-inc" %% "play2-oauth2-provider" % scalaOauth2Version,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.play" %% "play-streams-experimental" % playVersion,
  "com.typesafe.play" %% "play-slick" % playSlickVersion,
  "com.typesafe.play" %% "play-slick-evolutions" % playSlickVersion,
  "com.typesafe.slick" %% "slick" % slickVersion,
  "com.github.tototoshi" %% "slick-joda-mapper" % slickJodaMapperVersion,
  "org.postgresql" % "postgresql" % pgDriverVersion
)

libraryDependencies ++= Seq(
  specs2 % Test,
  "org.scalatest" % "scalatest_2.11" % scalatestVersion % Test,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "org.mockito" % "mockito-core" % mockitoVersion % Test,
  "de.leanovate.play-mockws" %% "play-mockws" % mockWsVersion % Test,
  "com.whisk" %% "docker-testkit-scalatest" % dockerTestkitVersion % Test,
  "com.whisk" %% "docker-testkit-impl-spotify" % dockerTestkitVersion % Test
)
