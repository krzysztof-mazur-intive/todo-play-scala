#Step #1: Build
FROM openjdk:8u151 as builder

ENV SCALA_VERSION 2.11.12
ENV SBT_VERSION 0.13.16

RUN touch /usr/lib/jvm/java-8-openjdk-amd64/release

RUN curl -fsL https://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz | tar xfz - -C /root/ && \
    echo >> /root/.bashrc && \
    echo "export PATH=~/scala-$SCALA_VERSION/bin:$PATH" >> /root/.bashrc

RUN curl -L -o sbt-$SBT_VERSION.deb https://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
    dpkg -i sbt-$SBT_VERSION.deb && \
    rm sbt-$SBT_VERSION.deb && \
    apt-get update && \
    apt-get install -y sbt

ENV BUILD_DIRECTORY /build

RUN mkdir $BUILD_DIRECTORY
RUN mkdir $BUILD_DIRECTORY/project

WORKDIR $BUILD_DIRECTORY

COPY app $BUILD_DIRECTORY/app
COPY conf $BUILD_DIRECTORY/conf
COPY project/build.properties $BUILD_DIRECTORY/project/build.properties
COPY project/plugins.sbt $BUILD_DIRECTORY/project/plugins.sbt
COPY build.sbt $BUILD_DIRECTORY/build.sbt

RUN sbt compile && sbt dist

ENV TARGET_DIRECTORY $BUILD_DIRECTORY/target/universal
ENV PROJECT_NAME todo-play-scala
ENV PROJECT_VERSION 0.1

RUN cd $TARGET_DIRECTORY && unzip $PROJECT_NAME-$PROJECT_VERSION.zip

#Step #2: Run
FROM openjdk:8u151

ENV BUILD_DIRECTORY /build
ENV TARGET_DIRECTORY $BUILD_DIRECTORY/target/universal
ENV PROJECT_NAME todo-play-scala
ENV PROJECT_VERSION 0.1
ENV APPLICATION_DIRECTORY /app
ENV USER_NAME scala
ENV USER_UID 567
ENV USER_GID 567

RUN set -x \
    && addgroup --gid ${USER_GID} ${USER_NAME} \
    && adduser --uid ${USER_UID} --gid ${USER_GID} ${USER_NAME} \
    && set +x

COPY --from=builder $TARGET_DIRECTORY/$PROJECT_NAME-$PROJECT_VERSION $APPLICATION_DIRECTORY

COPY entrypoint.sh $APPLICATION_DIRECTORY/bin/entrypoint.sh
RUN chmod +x $APPLICATION_DIRECTORY/bin/entrypoint.sh

ENTRYPOINT ["app/bin/entrypoint.sh"]

RUN chown $USER_NAME:$USER_NAME -R $APPLICATION_DIRECTORY

USER $USER_NAME

CMD ["/app/bin/todo-play-scala", "-Dplay.evolutions.db.default.autoApply=true"]
