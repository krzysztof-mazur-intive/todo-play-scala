#!/usr/bin/env bash

[ -e /app/RUNNING_PID ] && rm /app/RUNNING_PID

exec "$@"
