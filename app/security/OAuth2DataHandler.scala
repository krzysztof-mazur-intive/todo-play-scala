package security

import java.util.Date

import javax.inject.Singleton
import play.api.libs.Crypto
import scalaoauth2.provider.{AccessToken, AuthInfo, AuthorizationRequest, DataHandler}

import scala.concurrent.Future

@Singleton
class OAuth2DataHandler extends DataHandler[Account] {

  private val accessTokenExpire = Some(24L * 60L * 60L)
  private var accessTokens = Map[String, AccessToken]()
  private var authInfos = Map[String, AuthInfo[Account]]()

  override def findAuthInfoByAccessToken(accessToken: AccessToken): Future[Option[AuthInfo[Account]]] =
    Future.successful(authInfos.get(accessToken.token))

  override def findAccessToken(token: String): Future[Option[AccessToken]] = {
    authInfos.get(token) match {
      case Some(authInfo) => Future.successful(accessTokens.get(key(authInfo.user.username, authInfo.clientId.get)))
      case None => Future.successful(None)
    }
  }

  override def validateClient(request: AuthorizationRequest): Future[Boolean] = Future.successful(true) //TODO

  override def findUser(request: AuthorizationRequest): Future[Option[Account]] = Future.successful(
    request.param("username").getOrElse(None) match {
      case "admin" if "pass".equals(request.param("password").getOrElse(None)) => Some(Account(username = "admin"))
      case _ => None
    }
  )

  override def createAccessToken(authInfo: AuthInfo[Account]): Future[AccessToken] = {
    val token = AccessToken(Crypto.generateToken, None, authInfo.scope, accessTokenExpire, new Date)
    save(authInfo, token)
    Future.successful(token)
  }

  override def getStoredAccessToken(authInfo: AuthInfo[Account]): Future[Option[AccessToken]] = Future.successful(
    accessTokens.get(key(authInfo.user.username, authInfo.clientId.get)) match {
      case Some(token) if token.scope.equals(authInfo.scope) => Some(token)
      case _ => None
    }
  )

  override def refreshAccessToken(authInfo: AuthInfo[Account], refreshToken: String): Future[AccessToken] = ???

  override def findAuthInfoByCode(code: String): Future[Option[AuthInfo[Account]]] = ???

  override def deleteAuthCode(code: String): Future[Unit] = ???

  override def findAuthInfoByRefreshToken(refreshToken: String): Future[Option[AuthInfo[Account]]] = ???

  private def save(authInfo: AuthInfo[Account], accessToken: AccessToken): Unit = {
    val username = authInfo.user.username
    val clientId = authInfo.clientId.get

    accessTokens += (key(username, clientId) -> accessToken)
    authInfos += (accessToken.token -> authInfo)
  }

  private def key(username: String, clientId: String) = s"$username:$clientId"
}
