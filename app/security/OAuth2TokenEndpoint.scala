package security

import javax.inject.Singleton
import scalaoauth2.provider._

@Singleton
class OAuth2TokenEndpoint extends TokenEndpoint {
  override val handlers: Map[String, GrantHandler] = Map(
    OAuthGrantType.PASSWORD -> new Password()
  )
}
