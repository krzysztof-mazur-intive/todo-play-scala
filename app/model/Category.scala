package model

case class Category(id: Option[Long] = None, name: String)
