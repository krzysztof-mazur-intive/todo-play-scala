package model

import java.sql.Timestamp
import java.time.LocalDateTime
import javax.inject.Inject

import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile

import scala.concurrent.Future

class ToDoRepository @Inject()(dbConfigProvider: DatabaseConfigProvider) {
  // We want the JdbcProfile for this provider
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import slick.driver.PostgresDriver.api._

  implicit val localDateToTimestamp = MappedColumnType.base[LocalDateTime, Timestamp](
    l => Timestamp.valueOf(l),
    t => t.toLocalDateTime
  )

  private class ToDosTable(tag: Tag) extends Table[ToDo](tag, "todos") {

    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def description = column[String]("description")

    def categoryId = column[Long]("category_id")

    def dueDate = column[LocalDateTime]("due_date")

    def created = column[LocalDateTime]("created")

    def * = (id, name, description, categoryId, dueDate, created) <> ((ToDo.apply _).tupled, ToDo.unapply)
  }

  private val toDos = TableQuery[ToDosTable]

  def findAll(categoryId: Option[Long]): Future[Seq[ToDo]] = db.run {
    toDos.filter { toDo => categoryId.fold(true.bind)(toDo.categoryId === _) }.result
  }

  def countExpired(): Future[Int] = db.run {
    toDos.filter { _.dueDate < LocalDateTime.now() }.length.result
  }

  def findOneById(id: Long): Future[Option[ToDo]] = db.run {
    toDos.filter(_.id === id).result.headOption
  }

  def create(toDo: ToDo): Future[Option[Long]] = db.run {
    (toDos returning toDos.map(_.id)) += toDo
  }

  def update(id: Long, name: String, description: String, categoryId: Long, dueDate: LocalDateTime): Future[Int] = db.run {
    toDos.filter(_.id === id)
      .map(toDo => (toDo.name, toDo.description, toDo.categoryId, toDo.dueDate))
      .update((name, description, categoryId, dueDate))
  }

  def delete(id: Long): Future[Int] = db.run {
    toDos.filter(_.id === id).delete
  }
}
