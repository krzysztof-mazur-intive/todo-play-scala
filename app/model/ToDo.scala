package model

import java.time.LocalDateTime

case class ToDo(id: Option[Long] = None, name: String, description: String, categoryId: Long, dueDate: LocalDateTime,
                created: LocalDateTime = LocalDateTime.now())
