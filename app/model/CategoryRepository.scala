package model

import javax.inject.{Inject, Singleton}

import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile

import scala.concurrent.Future

@Singleton
class CategoryRepository @Inject()(dbConfigProvider: DatabaseConfigProvider) {
  // We want the JdbcProfile for this provider
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import dbConfig.driver.api._

  private class CategoriesTable(tag: Tag) extends Table[Category](tag, "categories") {

    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def * = (id, name) <> ((Category.apply _).tupled, Category.unapply)
  }

  private val categories = TableQuery[CategoriesTable]

  def all(): Future[Seq[Category]] = db.run {
    categories.result
  }

  def create(category: Category): Future[Option[Long]] = db.run {
    (categories returning categories.map(_.id)) += category
  }
}
