package controllers

import actors.ToDoRepositoryActor
import actors.ToDoRepositoryActor.CountExpired
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import javax.inject.Inject
import model.ToDoRepository
import play.api.mvc._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class SSEController @Inject()(system: ActorSystem, executionContext: ExecutionContext, repository: ToDoRepository)
  extends Controller with ScalaTicker with AkkaStreamsConversion {

  implicit val materializer: ActorMaterializer = ActorMaterializer()(system)
  implicit val timeout: Timeout = 10.seconds
  implicit val ec: ExecutionContext = executionContext

  private val toDoRepositoryActor = system.actorOf(ToDoRepositoryActor.props(repository))

  def countExpired() = Action {
    Ok.chunked(sourceToEnumerator(stringSource(expiredCountingFunction))).as(EVENT_STREAM)
  }

  val expiredCountingFunction: String => Future[String] = tick =>
    (toDoRepositoryActor ? CountExpired).mapTo[Int].map(count => count.toString)
}
