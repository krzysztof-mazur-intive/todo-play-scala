package controllers

import javax.inject.Inject

import actors.ToDoRepositoryActor
import actors.ToDoRepositoryActor._
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import controllers.json.ToDoJson
import dto.{CreateToDoDto, ToDoDto, UpdateToDoDto}
import model.{ToDo, ToDoRepository}
import play.api.data.validation.ValidationError
import play.api.libs.json._
import play.api.mvc.{Action, AnyContent, Controller, Result}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class ToDoController @Inject()(system: ActorSystem, executionContext: ExecutionContext, repository: ToDoRepository)
  extends Controller with ToDoJson {

  implicit val timeout: Timeout = 10.seconds
  implicit val ec: ExecutionContext = executionContext

  private val toDoRepositoryActor = system.actorOf(ToDoRepositoryActor.props(repository))

  private val validationErrorsMapper: Seq[(JsPath, Seq[ValidationError])] => Future[Result] = errors => Future {
    BadRequest(Json.obj("status" -> "error", "message" -> JsError.toJson(errors)))
  }

  private def createNotFound(id: Long): Result = {
    NotFound(Json.obj("status" -> "notFound", "message" -> s"ToDo item with ID $id not found"))
  }

  def index(categoryId: Option[Long]): Action[AnyContent] = Action.async {
    (toDoRepositoryActor ? GetAll(categoryId)).mapTo[Seq[ToDo]].map { toDos =>
      Ok(Json.toJson(toDos.map(t => ToDoDto(t.id, t.name, t.description, t.categoryId, t.dueDate, t.created))))
    }
  }

  def single(id: Long): Action[AnyContent] = Action.async {
    (toDoRepositoryActor ? GetOneById(id)).mapTo[Option[ToDo]].map(option => {
      option.fold(createNotFound(id)) { t =>
        Ok(Json.toJson(ToDoDto(t.id, t.name, t.description, t.categoryId, t.dueDate, t.created)))
      }
    })
  }

  def create: Action[JsValue] = Action.async(parse.json) { request =>
    val createResult = request.body.validate[CreateToDoDto]
    createResult.fold(validationErrorsMapper, valid => {
      (toDoRepositoryActor ? Create(valid.name, valid.description, valid.categoryId, valid.dueDate)).mapTo[Option[Long]]
        .map {
          case Some(id) => Created(s"$id")
          case None => InternalServerError(Json.toJson("Server error!"))
        }.recover({ case _ => InternalServerError(Json.toJson("Server error!")) })
    })
  }

  def delete(id: Long): Action[AnyContent] = Action.async {
    (toDoRepositoryActor ? Delete(id)).mapTo[Int].map { deleted =>
      if (deleted == 1) NoContent else createNotFound(id)
    }.recover({ case _ => InternalServerError(Json.toJson("Server error!")) })
  }

  def update(id: Long): Action[JsValue] = Action.async(parse.json) { request =>
    val updateResult = request.body.validate[UpdateToDoDto]
    updateResult.fold(validationErrorsMapper, valid => {
      (toDoRepositoryActor ? Update(id, valid.name, valid.description, valid.categoryId, valid.dueDate)).mapTo[Int]
        .map { updated =>
          if (updated == 1) NoContent else createNotFound(id)
        }.recover({ case _ => InternalServerError(Json.toJson("Server error!")) })
    })
  }
}
