package controllers

import javax.inject.Inject
import actors.JSONPlaceholderActor
import actors.JSONPlaceholderActor.GetPhotos
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import dto.PhotoDto
import play.api.libs.json._
import play.api.libs.ws.WSClient
import play.api.mvc.{Action, AnyContent, Controller}
import scalaoauth2.provider.OAuth2Provider
import security.OAuth2DataHandler

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

class PhotoController @Inject()(system: ActorSystem, ws: WSClient, executionContext: ExecutionContext,
                                dataHandler: OAuth2DataHandler) extends Controller with OAuth2Provider {

  implicit val timeout: Timeout = 10.seconds
  implicit val ec: ExecutionContext = executionContext

  implicit val photoWrites: Writes[PhotoDto] = Json.writes[PhotoDto]

  private val jsonPlaceholderActor = system.actorOf(JSONPlaceholderActor.props(ws))

  def index: Action[AnyContent] = Action.async { implicit request =>
    authorize(dataHandler) { authInfo =>
      (jsonPlaceholderActor ? GetPhotos).mapTo[Seq[PhotoDto]].map { photos =>
        Ok(Json.toJson(photos))
      }.recover({ case _ => InternalServerError(Json.toJson("Server error!")) })
    }
  }
}
