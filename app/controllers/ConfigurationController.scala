package controllers

import javax.inject.Inject

import play.Configuration
import play.api.mvc.{Action, Controller, Result}

class ConfigurationController @Inject()(conf: Configuration) extends Controller {

  def customProperty = Action {
    Ok(conf.getString("todo.customProperty"))
  }
}
