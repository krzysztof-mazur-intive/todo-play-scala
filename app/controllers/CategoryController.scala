package controllers

import javax.inject.Inject

import actors.CategoryRepositoryActor
import actors.CategoryRepositoryActor.{Create, GetAll}
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import controllers.json.CategoryJson
import dto.{CategoryDto, CreateCategoryDto}
import model.{Category, CategoryRepository}
import play.api.cache.CacheApi
import play.api.data.validation.ValidationError
import play.api.libs.json._
import play.api.mvc.{Action, AnyContent, Controller, Result}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class CategoryController @Inject()(system: ActorSystem, executionContext: ExecutionContext, repository: CategoryRepository, cache: CacheApi)
  extends Controller with CategoryJson {

  implicit val timeout: Timeout = 10.seconds
  implicit val ec: ExecutionContext = executionContext

  private val categoryRepositoryActor = system.actorOf(CategoryRepositoryActor.props(repository, cache))

  private val validationErrorsMapper: Seq[(JsPath, Seq[ValidationError])] => Future[Result] = errors =>
    Future {
      BadRequest(Json.obj("status" -> "error", "message" -> JsError.toJson(errors)))
    }

  def index: Action[AnyContent] = Action.async {
    (categoryRepositoryActor ? GetAll).mapTo[Seq[Category]].map { categories =>
      Ok(Json.toJson(categories.map(c => CategoryDto(c.id, c.name))))
    }
  }

  def create: Action[JsValue] = Action.async(parse.json) { request =>
    val createResult = request.body.validate[CreateCategoryDto]
    createResult.fold(validationErrorsMapper, valid => {
      (categoryRepositoryActor ? Create(valid.name)).mapTo[Option[Long]].map {
        case Some(id) => Created(s"$id")
        case None => InternalServerError(Json.toJson("Server error!"))
      }.recover({ case _ => InternalServerError(Json.toJson("Server error!")) })
    });
  }
}
