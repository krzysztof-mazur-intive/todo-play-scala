package controllers.json

import dto.{CategoryDto, CreateCategoryDto}
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

trait CategoryJson {
  implicit val categoryWrites: Writes[CategoryDto] = Json.writes[CategoryDto]
  implicit val categoryReads: Reads[CategoryDto] = Json.reads[CategoryDto]
  implicit val createCategoryReads: Reads[CreateCategoryDto] =
    (JsPath \ "name").read[String](minLength[String](3) andKeep maxLength[String](50)).map(name => CreateCategoryDto(name))
  implicit val createCategoryWrites: Writes[CreateCategoryDto] = Json.writes[CreateCategoryDto]
}
