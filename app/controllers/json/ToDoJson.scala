package controllers.json

import java.time.LocalDateTime

import dto.{CreateToDoDto, ToDoDto, UpdateToDoDto}
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

trait ToDoJson {

  implicit val todoWrites: Writes[ToDoDto] = Json.writes[ToDoDto]
  implicit val createToDoReads: Reads[CreateToDoDto] = (
    (JsPath \ "name").read[String](minLength[String](3) andKeep maxLength[String](100)) and
      (JsPath \ "description").read[String] and
      (JsPath \ "categoryId").read[Long] and
      (JsPath \ "dueDate").read[LocalDateTime]
    ) (CreateToDoDto.apply _)
  implicit val updateToDoReads: Reads[UpdateToDoDto] = Json.reads[UpdateToDoDto]
}
