package controllers

import akka.stream.scaladsl.Source

import scala.concurrent.Future
import scala.concurrent.duration._

trait ScalaTicker {

  def stringSource(source: String => Future[String]): Source[String, _] = {
    val tickSource = Source.tick(0.millis, 5000.millis, "tick")
    val s = tickSource.mapAsync(1)(source)
    s
  }
}
