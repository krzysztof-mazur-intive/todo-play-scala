package controllers

import akka.stream.Materializer
import akka.stream.scaladsl._
import org.reactivestreams._
import play.api.libs.iteratee._
import play.api.libs.streams.Streams

trait AkkaStreamsConversion {
  def sourceToEnumerator[Out, Mat](source: Source[Out, Mat])(implicit fm: Materializer): Enumerator[Out] = {
    val pubr: Publisher[Out] = source.runWith(Sink.asPublisher[Out](fanout = true))
    Streams.publisherToEnumerator(pubr)
  }
}
