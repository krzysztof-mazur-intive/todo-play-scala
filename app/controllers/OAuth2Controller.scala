package controllers

import javax.inject.Inject
import play.api.mvc.{Action, AnyContent, Controller}
import scalaoauth2.provider.OAuth2Provider
import security.{OAuth2DataHandler, OAuth2TokenEndpoint}

import scala.concurrent.ExecutionContext

class OAuth2Controller @Inject()(dataHandler: OAuth2DataHandler, executionContext: ExecutionContext) extends Controller
  with OAuth2Provider {

  implicit val ec: ExecutionContext = executionContext

  override val tokenEndpoint = new OAuth2TokenEndpoint

  def token: Action[AnyContent] = Action.async { implicit request =>
    issueAccessToken(dataHandler)
  }
}
