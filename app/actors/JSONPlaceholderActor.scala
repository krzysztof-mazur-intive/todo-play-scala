package actors

import javax.inject.Inject

import akka.pattern.pipe
import akka.actor.{Actor, ActorLogging, Props}
import dto.PhotoDto
import play.api.libs.json.{Json, Reads}
import play.api.libs.ws.WSClient

import scala.concurrent.{ExecutionContextExecutor, Future}

object JSONPlaceholderActor {

  def props(ws: WSClient): Props = Props(new JSONPlaceholderActor(ws))

  case class GetPhotos()

}

class JSONPlaceholderActor @Inject()(ws: WSClient) extends Actor with ActorLogging {

  import actors.JSONPlaceholderActor._

  private val photosEndpoint: String = "https://jsonplaceholder.typicode.com/photos"

  implicit val ec: ExecutionContextExecutor = context.dispatcher
  implicit val photoDtoReads: Reads[PhotoDto] = Json.reads[PhotoDto]

  override def receive: Receive = {
    case GetPhotos => pipe(getPhotos) to sender()
  }

  private def getPhotos: Future[Seq[PhotoDto]] = ws.url(photosEndpoint).get().map { response =>
    response.json.validate[Seq[PhotoDto]].get
  }
}
