package actors

import javax.inject.Inject

import akka.pattern.pipe
import akka.actor.{Actor, ActorLogging, Props}
import model.{Category, CategoryRepository}
import play.api.Logger
import play.api.cache.CacheApi

import scala.concurrent.{ExecutionContextExecutor, Future}

object CategoryRepositoryActor {

  def props(repository: CategoryRepository, cacheApi: CacheApi): Props =
    Props(new CategoryRepositoryActor(repository, cacheApi))

  case class GetAll()

  case class Create(name: String)

}

class CategoryRepositoryActor @Inject()(repository: CategoryRepository, cache: CacheApi) extends Actor with ActorLogging {

  object CacheKey extends Enumeration {

    protected case class Val(key: String)

    val Categories = Val("categories")

  }

  import actors.CategoryRepositoryActor._
  import repository._

  implicit val ec: ExecutionContextExecutor = context.dispatcher

  override def receive: Receive = {
    case GetAll => pipe(allCached()) to sender()
    case Create(name) => pipe(createAndInvalidateCache(Category(name = name))) to sender()
  }

  private def allCached(): Future[Seq[Category]] = {
    allCachedOption().getOrElse {
      Logger.info("Categories not found in cache.")
      val categories = all()
      cache.set(CacheKey.Categories.key, categories)
      categories
    }
  }

  private def allCachedOption(): Option[Future[Seq[Category]]] = {
    cache.get[Future[Seq[Category]]](CacheKey.Categories.key)
  }

  private def createAndInvalidateCache(category: Category): Future[Option[Long]] = {
    cache.remove(CacheKey.Categories.key)
    create(category)
  }
}