package actors

import java.time.LocalDateTime
import javax.inject.Inject

import akka.pattern.pipe
import akka.actor.{Actor, ActorLogging, Props}
import model.{ToDo, ToDoRepository}

import scala.concurrent.ExecutionContextExecutor

object ToDoRepositoryActor {

  def props(repository: ToDoRepository): Props = Props(new ToDoRepositoryActor(repository))

  case class GetAll(categoryId: Option[Long])

  case class CountExpired()

  case class GetOneById(id: Long)

  case class Create(name: String, description: String, categoryId: Long, dueDate: LocalDateTime)

  case class Update(id: Long, name: String, description: String, categoryId: Long, dueDate: LocalDateTime)

  case class Delete(id: Long)

}

class ToDoRepositoryActor @Inject()(repository: ToDoRepository) extends Actor with ActorLogging {

  import actors.ToDoRepositoryActor._
  import repository._

  implicit val ec: ExecutionContextExecutor = context.dispatcher

  override def receive: Receive = {
    case GetAll(categoryId) => pipe(findAll(categoryId)) to sender()
    case CountExpired => pipe(countExpired()) to sender()
    case GetOneById(id) => pipe(findOneById(id)) to sender()
    case Create(name, description, categoryId, dueDate) =>
      pipe(create(ToDo(name = name, description = description, categoryId = categoryId, dueDate = dueDate))) to sender()
    case Update(id, name, description, categoryId, dueDate) =>
      pipe(update(id, name, description, categoryId, dueDate)) to sender()
    case Delete(id) => pipe(delete(id)) to sender()
  }
}
