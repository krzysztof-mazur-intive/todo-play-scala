package dto

import java.time.LocalDateTime

case class UpdateToDoDto(name: String, description: String, categoryId: Long, dueDate: LocalDateTime)
