package dto

import java.time.LocalDateTime

case class ToDoDto(id: Option[Long], name: String, description: String, categoryId: Long, dueDate: LocalDateTime,
                   created: LocalDateTime)
