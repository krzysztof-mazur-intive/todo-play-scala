package dto

case class PhotoDto(id: Long, albumId: Long, title: String, url: String, thumbnailUrl: String)
