package dto

case class CategoryDto(id: Option[Long] = None, name: String)
