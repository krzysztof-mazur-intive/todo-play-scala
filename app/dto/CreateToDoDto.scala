package dto

import java.time.LocalDateTime

case class CreateToDoDto(name: String, description: String, categoryId: Long, dueDate: LocalDateTime)
